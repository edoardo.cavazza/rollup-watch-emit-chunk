# rollup-watch-emit-chunk

An example of a Rollup issue when using incremental build and plugins which emit chunks.

## Reproduce the issue

* Install the dependencies

    ```
    $ npm install
    ```

* Run rollup in watch mode

    ```
    $ rollup -c rollup.config.js --watch
    ```

* First build is ok!
* Open `src/main.js` and save (it triggers a rebuild)
* Build fails with error: `Error: Plugin error - Unable to get file name for unknown chunk "e761ca66".`