const REGISTER_WORKLET = 'register-paint-worklet:';

export default {
    input: 'src/main.js',
    output: {
        format: 'esm',
        dir: 'dist',
    },
    plugins: [
        {
            load(id) {
                if (id.startsWith(REGISTER_WORKLET)) {
                    return 'CSS.paintWorklet.addModule(PLACEHOLDER);';
                }
            },
            transform(code, id) {
                if (id.startsWith(REGISTER_WORKLET)) {
                    return code.replace('PLACEHOLDER', `import.meta.ROLLUP_CHUNK_URL_${this.emitChunk(
                        id.slice(REGISTER_WORKLET.length)
                    )}`);
                }
            },
            resolveId(id, importee) {
                if (id.startsWith(REGISTER_WORKLET)) {
                    return this.resolveId(id.slice(REGISTER_WORKLET.length), importee).then(
                        id => REGISTER_WORKLET + id
                    );
                }
                return null;
            },
        }
    ],
}